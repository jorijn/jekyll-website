---
title: Diensten
permalink: diensten/
profile: true
---

## Front-end ontwikkelaar
HTML heeft weinig geheimen meer voor mij. Ik kan helpen met het vertalen van een ontwerp naar een bruikbare website of met het implementeren van moderne web technologieën zoals HTML5 en CSS3. Javascript is geen probleem, ik heb gewerkt met NodeJS, jQuery en AngularJS (1.x).

## Back-end ontwikkelaar
Buiten advies en ontwerp van een applicatie kan ik ook helpen bij de realisatie hiervan. Ik ben door het aanleren van programmeer standaarden (OOP) in te zetten in teamverband of als zelfstandig persoon. Mijn specialisatie ligt bij web-applicaties opgebouwd met PHP in CodeIgniter, Kahona, Laravel, CakePHP of Symfony2. Mijn framework van keuze is Symfony2 of Laravel (gebaseerd op Symfony componenten). Ik heb meerdere projecten gedaan waarbij ik Wordpress heb ingezet als fundering voor een maatwerk CMS met custom-made plug-ins en thema’s.

## Teamplayer
Flexibel, doch serieus aan het werk. ’s Ochtends een kop koffie bij de stand-up en de rest van de dag samen met het Agile team knallen. Scrum, Kanban – het is geen probleem. Ik hou mijn voortgang bij in tools als JIRA of ActiveCollab.


{% include footer.html %}
