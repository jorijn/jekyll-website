---
title: Projecten en referenties
permalink: projecten/
profile: true
---

### Projecten en referenties
_Heb je een leuke opdracht voor me of ben je enkel op zoek naar een wat technischer beeld van mijn kunnen? Download dan mijn [Curriculum Vitae](/assets/cv.pdf)._ 

Een kleine greep uit mijn projecten en klanten. Kijk op mijn [LinkedIn](https://nl.linkedin.com/in/jorijnschrijvershof) voor een algemener idee.

* UWV <sup>2</sup> — [werk.nl](http://werk.nl)
* SBS Broadcasting <sup>1</sup> — [sbs6.nl](http://www.sbs6.nl), [net5.nl](http://www.net5.nl), [veronicatv.nl](http://www.veronicatv.nl), [kijk.nl](http://www.kijk.nl), [nlziet.kijk.nl](http://nlziet.kijk.nl)
* Nike <sup>2</sup>
* Kieskeurig <sup>1</sup> — o.a. [kieskeurig.nl](http://www.kieskeurig.nl)
* Sanoma Media <sup>1</sup> — cookie melding op o.a. [NU.nl](http://nu.nl), [AutoWeek.nl](http://autoweek.nl), zie [hier](http://www.sanoma.nl/merken/)
* Terberg Leasing <sup>3</sup> — [leasen.nl](http://www.leasen.nl), [justlease.nl](http://www.justlease.nl), [terbergleasing.nl](http://www.terbergleasing.nl)
* Betaalvereniging Nederland <sup>3</sup>
* Kwaliteitsinstituut Nederlandse Gemeenten (KING)<sup>2</sup>
* Diverse kleine projecten om te proberen en te leren — [Bitbucket pagina](http://bitbucket.org/jorijn)

<div class="ism-text">
    <sup>1</sup> in samenwerking met <a href="http://www.lajos.nl">Lajos B.V.</a><br>
    <sup>2</sup> in samenwerking met <a href="http://www.ebrella.nl">eBrella</a><br>
    <sup>3</sup> in samenwerking met <a href="http://www.clearsite.nl">Clearsite</a>
</div>

<script>
    jQuery(function() {
        jQuery('#wrapper a').attr('target', '_blank');
    });
</script>

{% include footer.html %}
